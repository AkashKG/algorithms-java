/*
Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?
*/

import java.util.*;

class Unique{

	public static boolean isUnique(String s){
		boolean MAP[] = new boolean[126];
		for(int i=0;i<s.length();i++){
			if(MAP[s.charAt(i)])
				return false;
			MAP[s.charAt(i)] = true;
		}
		return true;
	}

	public static void main(String[] args){
		String s;
		Scanner cin = new Scanner(System.in);
		s = cin.next();
		if(isUnique(s))
			System.out.println("Unique");
		else
			System.out.println("Not Unique");
	}

}
