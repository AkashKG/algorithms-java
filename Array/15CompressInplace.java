import java.util.*;

class Compress{
	
	public static int compressedLength(String s){
		if(s == null || s.isEmpty())
			return 0;
		char last = s.charAt(0);
		int size = 0, count = 1;
		for(int i=1;i<s.length();i++){
			if(s.charAt(i)==last)
				count++;
			else{
				last = s.charAt(i);
				size += 1 + String.valueOf(count).length();
				count = 1;
			}
		}
		return size;
	}
	
	public static String compress(String s){
		if(s.length()<compressedLength(s))
			return s;
		StringBuffer sb = new StringBuffer();
		char last = s.charAt(0);
		int count = 1;
		for(int i=1;i<s.length();i++){
			if(s.charAt(i)==last){
				count++;
			}
			else{
				sb.append(last);
				sb.append(String.valueOf(count));
				last = s.charAt(i);
				count = 1;
			}
		}
		sb.append(last);
		sb.append(String.valueOf(count));
		return sb.toString();
	}	

	public static void main(String[] args){
		String s = args[0];
		System.out.println(compress(s));	
	}
}
