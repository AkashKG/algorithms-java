/*
@author: Akash Kumar Gupta
@Date : 5:37PM, 20 Oct 16

Question: Given two strings, write a method to decide if one is a permutation of the other.
*/

import java.util.*;

class Permute{
	
	public static boolean permute(String s1, String s2){
		if(s1.length()!=s2.length()) 
			return false;
		Hashtable<Character, Integer> ht = new Hashtable<Character, Integer>();
		for(int i=0;i<s1.length();i++){
			if(ht.containsKey(s1.charAt(i))){
				ht.put(s1.charAt(i), ht.get(s1.charAt(i)) + 1);		//Incrementing the Value of the key.	
			}
			else ht.put(s1.charAt(i), 1);
		}		
		for(int i=0;i<s2.length();i++){
			if(ht.containsKey(s2.charAt(i))){
				ht.put(s2.charAt(i),ht.get(s2.charAt(i))-1);		//Once Found, Decrementing The Value.
				if(ht.get(s2.charAt(i))==0) 
					ht.remove(s2.charAt(i));
			}
			else return false;
		}
		return ht.isEmpty();
	}
	
	public static void main(String[] args){
		String str1, str2;
		Scanner sc = new Scanner(System.in);
		str1 = sc.next();
		str2 = sc.next();
		if(permute(str1, str2)) System.out.println("Permute Of Each Other");
		else System.out.println("Different words!");
	}
}
