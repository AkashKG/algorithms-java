/*

Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

*/


import java.util.*;
class UniqueHash{
	public static void main(String[] args){
		String s;
		Scanner sc = new Scanner(System.in);
		s = sc.next();
		String answer = "This is Unique";
		HashSet<Character> hs = new HashSet<Character>();
		for(int i=0;i<s.length();i++){
			if(hs.add(s.charAt(i))==false){
				answer = "This is not Unique";
			}

		}
		System.out.println(answer);
	}
}
