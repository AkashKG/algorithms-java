import java.util.*;

class DutchNationalFlag{
	int arr[];
	int len;
	
	DutchNationalFlag(int len){
		Scanner sc = new Scanner(System.in);
		this.len = len;
		arr = new int[this.len];
		for(int i=0;i<this.len;i++){
			arr[i] = sc.nextInt();
		}
	}

	void DNFAlgo(){
		int low = 0, mid = 0, high = len-1;
		while(mid<high){
			switch(arr[mid]){
				case 0: arr[low] = arr[low] ^ arr[mid] ^ (arr[mid] = arr[low]);
					low++;
					break;
				case 1:	mid++;
					break;
				case 2:	arr[high] = arr[high] ^ arr[mid] ^ (arr[mid]=arr[high]);
					high--;
					break;
			}
		}
	}

	void displayArray(){
		for(int i=0;i<len;i++){
			System.out.print(arr[i] + " ");
		}
	}

}

class DriverProg{
	public static void main(String[] args){
		DutchNationalFlag dnf = new DutchNationalFlag(Integer.parseInt(args[0]));
		dnf.DNFAlgo();
		dnf.displayArray();		
	}	
}
