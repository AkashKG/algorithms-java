/*This is the main program*/

import java.util.*;

public class StackWithMin extends Stack<Integer>{
	Stack<Integer> st;

	public StackWithMin(){
		st = new Stack<Integer>();
	}

	public void push(int value){
		if(value<=min()){
			st.push(value);
		}
		super.push(value);
	}

	public Integer pop(){
		int value = super.pop();
		if(value == min()){
			st.pop();
		}
		return value;
	}

	public int min(){
		if(st.isEmpty()){
			return Integer.MAX_VALUE;
		}
		return st.peek();
	}

}
