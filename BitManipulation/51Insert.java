import java.util.*;

class Insert{
	public static int clearBit(int N, int i){
		int mask = ~(1<<i);
		return N & mask;
	}
	public static int insertAndConvert(int N, int M, int start, int end){
		for(int i=start;i<=end;i++)
			N = clearBit(N, i);
		M=M<<start;
		return N|M;
	}
	public static void main(String[] args){
		int N, M, start, end;
		Scanner sc = new Scanner(System.in);
		N = sc.nextInt();
		System.out.println(Integer.toBinaryString(N));
		M = sc.nextInt();
		System.out.println(Integer.toBinaryString(M));
		start = sc.nextInt();
		end = sc.nextInt();

		N = insertAndConvert(N, M, start, end);
		System.out.println(Integer.toBinaryString(N));
		
	}
}
