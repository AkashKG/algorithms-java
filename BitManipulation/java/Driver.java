import java.util.*;

class Driver{
	public static void main(String[] args){
		GSCU g = new GSCU(Integer.parseInt(args[0]));
		System.out.println(g.getBit(Integer.parseInt(args[1])));
		System.out.println(g.clearBit(Integer.parseInt(args[1])));
		System.out.println(g.setBit(Integer.parseInt(args[1])));
		System.out.println(g.updateBit(Integer.parseInt(args[1]),Integer.parseInt(args[2])));
	}
}
