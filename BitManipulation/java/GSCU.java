import java.util.*;

public class GSCU{
	int num;
	
	GSCU(int num){
		this.num = num;
	}
		
	boolean getBit(int i){
		return (num&(1<<i))!=0;	
	}
	
	int clearBit(int i){
		int mask = ~(1<<i);
		num = num & mask;
		return num;
	}

	int setBit(int i){
		num = num | (1<<i);
		return num;
	}

	int updateBit(int i, int v){
		int mask = ~(1<<i);
		num = (num & mask) | (v<<i);
		return num;
	}
}
