import java.util.*;

class DoubleIn32Bit{

	public static String printBinary(double num){
		if(num<=0||num>=1)
			return "Error";
		StringBuilder binary = new StringBuilder();
		double frac = 0.5;
		binary.append('.');
		while(num>0){
			if(binary.length()>32)
				return (binary.append("Exceeded!")).toString();
			if(num>=frac){
				binary.append(1);
				num = num - frac;
			}
			else
				binary.append(0);
			frac/=2;	
		
		}
		return binary.toString();
	}

	public static void main(String[] args){
		double value = 0.75;
		System.out.println(printBinary(value));
	}

}
