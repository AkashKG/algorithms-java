#include<iostream>
using namespace std;

class GSCU{
	int num;
	public:
	GSCU(int num){
		this->num = num;
	}
	bool updateNumber(int num);
	bool getBit(int i);
	int setBit(int i);
	int clearBit(int i);
	int updateBit(int i, int v);
};

bool GSCU::updateNumber(int num){
	this->num = num;
	return true;
}

bool GSCU::getBit(int i){
	return ((num&(1<<i)) != 0);
}

int GSCU::setBit(int i){
	num = num | (1<<i);
	return num;
}

int GSCU::clearBit(int i){
	int mask = ~(1<<i);
	num = num & mask;
	return num;
}

int GSCU::updateBit(int i, int v){
	int mask = ~(1<<i);
	num = (num & mask) | (v << i);
	return num;
}

int main(){
	GSCU *g = new GSCU(12);
	cout<<g->getBit(2)<<endl;
	g->updateNumber(33);
	cout<<g->updateBit(1,0)<<endl;
	cout<<g->setBit(3)<<endl;
	cout<<g->getBit(3)<<endl;
	cout<<g->clearBit(3)<<endl;
	return 0;
}
